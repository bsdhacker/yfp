package me.empornium.mendilo.yfp;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/*
 * Run via java -Xdock:name=Extractor -jar yfp-0.0.1-SNAPSHOT-jar-with-dependencies.jar
 */

public class MainFrame extends JFrame implements Runnable, ActionListener {

    private static final Pattern ID_PATTERN = Pattern.compile("http://[^/]+/video/([0-9]+)/.*?");
    private static final String PLAYLIST = "http://www.yourfreeporn.us/media/nuevo/playlist.php?key=";
    private static final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:20.0) Gecko/20100101 Firefox/20.0";

    private static final long serialVersionUID = 1L;

    private final JPanel contentPane;
    private final JTextArea txtLinks;
    private final JButton btnResolve;
    private final JPanel btnPanel;
    private final JLabel lblStatus;

    public MainFrame() {
        super("Your Free Porn Link Extractor");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 650, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

        txtLinks = new JTextArea();
        txtLinks.setText("Add each link separated by a new line here.");
        txtLinks.setBorder(BorderFactory.createLoweredBevelBorder());
        txtLinks.setRows(13);
        txtLinks.setColumns(100);
        final JScrollPane scroll = new JScrollPane(txtLinks);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        contentPane.add(scroll);

        btnPanel = new JPanel();
        btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));

        lblStatus = new JLabel("");
        lblStatus.setHorizontalAlignment(SwingConstants.LEFT);
        btnPanel.add(lblStatus);

        btnPanel.add(Box.createGlue());

        btnResolve = new JButton("Resolve Links");
        btnResolve.addActionListener(this);

        btnPanel.add(btnResolve);

        contentPane.add(btnPanel);
    }

    public static void main(String[] args) throws Exception {
        EventQueue.invokeLater(new MainFrame());
    }

    @Override
    public void run() {
        try {
            MainFrame frame = new MainFrame();
            frame.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        final List<String> links = getLinks(txtLinks.getText());
        if (links.size() == 0) {
            JOptionPane.showMessageDialog(this, "Please enter some links", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        txtLinks.setText("");
        for (int i = 0; i < links.size(); i++) {
            lblStatus.setText("Working on link " + (i + 1) + " of " + links.size());

            try {
                final String location = resolve(links.get(i));
                txtLinks.append(location + System.lineSeparator());
            } catch (IOException | SAXException | IllegalStateException | ParserConfigurationException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        lblStatus.setText("Resolved " + links.size() + " links");
    }

    private static List<String> getLinks(String text) {
        final List<String> result = new ArrayList<>();
        String[] lines = text.split(System.lineSeparator());
        for (String line : lines) {
            final String trimmed = line.trim();
            if (trimmed.length() != 0)
                result.add(line);
        }
        return result;
    }

    public static String resolve(String link) throws IOException, SAXException, ParserConfigurationException {
        final int id = getIdFromLink(link);
        final String xml = getUrl(PLAYLIST + id);
        return extractFile(xml);
    }

    private static int getIdFromLink(String link) {
        final Matcher matcher = ID_PATTERN.matcher(link);
        if (matcher.matches())
            return Integer.parseInt(matcher.group(1));

        throw new IllegalStateException("Could not extract id from link: " + link);
    }

    private static String extractFile(String xml) throws IOException, SAXException, ParserConfigurationException {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder builder = factory.newDocumentBuilder();
        try (InputStream stream = new ByteArrayInputStream(xml.getBytes())) {
            final Document doc = builder.parse(stream);
            final Node file = doc.getElementsByTagName("file").item(0);
            return file.getTextContent();
        }
    }

    private static String getUrl(String location) throws IOException {
        final URL url = new URL(location);
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setReadTimeout(7000);
        connection.setConnectTimeout(7000);
        connection.setRequestMethod("GET");
        connection.setDoInput(true);
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.connect();

        final int code = connection.getResponseCode();
        if (code != 200)
            throw new IOException("HTTP GET not ok: " + code);

        try (InputStream stream = connection.getInputStream(); Scanner scanner = new Scanner(stream)) {
            scanner.useDelimiter("\\A");

            if (scanner.hasNext())
                return scanner.next();

            throw new IOException("No content");
        }
    }
}
