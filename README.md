# yourfreeporn.us resolver

Download the runnable `.jar` from the [downloads section](https://bitbucket.org/bsdhacker/yfp/downloads) of the project and launch it via:

    # java -jar yfp-0.0.1-SNAPSHOT-jar-with-dependencies.jar

Enter a list of the videos you which to download:

![image](http://i.imgur.com/pC53FZP.png)


And hit the `Resolve` button:

![image](http://i.imgur.com/Bg7KdAI.png)

and you get a link to a `flv file` for each video link entered. You can download these video files via any browser or your favorite download manager.

---

### Building from source

Simply use maven to build a runnable jar from source:

    # mvn clean package
    
